# B0B39KAJ - Semestrální práce
Autor: Antonín Mašek
Semestr: Letní semestr 2019

## Zadání
Jako téma své semestrální práce jsem si zvolil hru **Snake**. Cílem bylo implementovat hru s pomocí frameworku [p5](https://p5js.org/). Na stránce lze také najít žebříček nejlepších 10 hráčů.

### Použité frameworky
- [p5](https://p5js.org/)
- [jQuery](https://jquery.com/)

## Kritéria hodnocení
V této sekci krátce shrnu určité body z tabulky hodnocení, které by mohly být snáz přehlédnutelné a aby je nebylo nutné těžce hledat.
- Input
    - Na stránce je umístěn jeden textový input ihned v první sekci, kde se po uživateli žádá zadání jména
    - Validace: Validace tohoto inputu probíhá v JS kódu, kde se vstupní text porovnává oproti regex výrazu `^[a-zA-Z0-9]{1,10}$`
        - V případě, že tato kritéria nejsou splněna dojde při ukládání výsledku k vygenerování náhodného řetěžce.
        - Validaci lze najít zde: *js/snake/state.js:98*
- Online/Offline
    - Jakmile je aplikace načtena, tak je schopna fungovat i v offline režimu
    - Detekce stavu:
        - Zda-li má aplikace přístup k internetu lze poznat podle "neonové cedule" ve 2. sekci v pravo od nápisu **Snake**. V případě, že má aplikace přístup k internetu, tak je cedule zelená a má nápis **Online**. V opačném případě je červená s nápisem **Offline**.
        - Detekce stavu je navěšena na eventy **online** a **offline** na window. Funkcionalita se nachází zde: *js/snake/entry.js:207*
        - Pro změnu zobrazení je použito přidání, či odebrání třídy `online`. Na základě existence této třídy se řídí i obsah, který se generuje pomocí css. *css/style.css:126*
- Pokročilé CSS selektory
    - Pokročilé selektory jsou k nalezení například zde: *css/style.css:225*
- CSS3 transformace
    - Jelikož mě v mém případě nenapadlo, kde bych mohl použít transformaci, tak jsem ji vytvořil jen pro ukázkové účely. Transformace se aplikuje na mé jméno po najetí myší. Jméno se nachází v patičce. A definice efektu zde: *css/style.css:284*.
- Pokročilá JS API
    - Z JS API využívám **LocalStorage** pro ukládání 10ti nejlepších hráčů. 
    - Dále využívám **Geolocation**. Toto API je využíváno opět spíše pro ukázkové účely. Při procesu ukládání se zároveň s hráčem pokusí uložit i jeho souřadnice. Děje se tak zde: *js/snake/state.js:187*.

## Struktura projektu
Projekt tvoří 3 základní složky: *js*, *css* a *resources*.

- *js*
    - *libraries*
        - Knihovny, které ke svému projektu využívám ([p5](https://p5js.org/))
            - [jQuery](https://jquery.com/) je natažena pomocí CDN
    - *main.js*
        - Zde jsou globální úpravy, které využívám napříč projektem
    - *snake*
        - Zde se nachází stavební bloky pro kompletní hru. Počáteční bod se nachází v *entry.js*
- *css*
    - Ve složce *css* se nachází styly. Aby nevznikl jeden nepřehledný soubor, tak jsem si stylování rozložil podle logických celků.
- *resources*
    - Potřebné podpůrné soubory pro projekt (obrázky, zvuky, fonty)

## Program
Zde hrubě popíši jak program funguje, aby v něm byla snazší orientace:

- Vstupní bod je soubor *js/snake/entry.js*, kde se nachází 2 hlavní metody knihovny [p5](https://p5js.org/) a to jsou:
    - `setup` - Tato metoda proběhne jednou a děje se v ní základní nastavení
    - `draw` - Tato metoda je opakovaně volána pro překreslení elementu canvas
    - V tomto souboru je deklarováno pár globáních proměnných, nachází se zde obsluha hry (ovládání šipkami) a registurjí se zde potřebné eventy
- Hlavní herní objekt je definován v *js/snake/game.js*. Tento objekt si drží informace o aktuálním stavu hry (reprezentováno objektem *js/snake/state.js*), dále drží objekt samotného hada *js/snake/snake.js* a pole s jídlem.
- **Stav** stav hry je udržován v objektu definovaném zde: *js/snake/state.js*. Tento objekt se zároveň stará o ukládání a načítaní do **LocalStorage**.
- **Jídlo** je definováno v *js/snake/food.js*. Má danou barvu a skóre (přičte se hráči po snědení). Rozměry a pozice jsou uloženy v objektu Tile *js/snake/tile.js*
- **Tile** - Jelikož jednotka hada **Cell** a jídlo **Food** mají stejnou základní funkcionalitu, tak jsem ji separoval v tomto objektu.
- **Had** je reprezentován objektem definovaným v *js/snake/snake.js*. Had je složen s Cell objektů *js/snake/cell.js*.
- **Cell** je jedna buňka hada.