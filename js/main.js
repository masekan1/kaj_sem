/**
* This event is registered to prevent scrolling the window, when controlling the snake with arrows.
* Source: https://stackoverflow.com/questions/8916620/disable-arrow-key-scrolling-in-users-browser
*/
window.addEventListener("keydown", function(e) {
    if([32, 38, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
    }
}, false);

/**
* Fix for modulo operation with negative numbers.
* Source: https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
*/
Number.prototype.mod = function(n) {
    return ((this % n) + n) % n;
};