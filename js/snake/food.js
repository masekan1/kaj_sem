/**
 * This object represents the food. It has score.
 */
class Food {

	constructor(position, score = 150) {
        // Save score which will be added to players score, when this food will be eaten
		this.score = score;

        // Create tile to hold color, position and size
		this.tile = new Tile(color(0, 183, 235), position);
	}

	/*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    */

    getTile() {
    	return this.tile;
    }

    getScore() {
        return this.score;
    }

    getPosition() {
    	return this.getTile().getPosition();
    }

    /*
    |--------------------------------------------------------------------------
    | Logic
    |--------------------------------------------------------------------------
    */

    /**
     * Recalculate the size of tiles
     */
    recalculate() {
        this.getTile().recalculate();
    }

     /*
    |--------------------------------------------------------------------------
    | Canvas
    |--------------------------------------------------------------------------
    */

    /**
     * Draw itself
     */
    draw() {
        this.getTile().draw();
    }
}