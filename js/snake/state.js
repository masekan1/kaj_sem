/**
 * This object represents the state of the game. It stores variables such as score, speed, ...
 * It also takes care of saving and retrieving data from LocalStorage.
 */
class State {

	constructor() {
		// Sets initial values for variables
		this.score = 0;
		this.speed = 0;
		this.paused = true;
		this.gameOver = false;

		// Leaderboard initial data
		this.setDate(this.getDateString());

		// Metadata
		this.sortData();
		this.saved = false;
	}

	/*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
	*/

	getDate() {
		return this.date;
	}

	getScore() {
		return this.score;
	}

	getSaved() {
		return this.saved;
	}

	getSpeed() {
		return this.speed;
	}

	isPaused() {
		return this.paused;
	}

	isGameOver() {
		return this.gameOver;
	}	

	getName() {
		// If the name is invalid, then return generated one
		if (!this.name || this.name.length <= 0 || this.name.length > 10) this.setName(this.randomName());
		return this.name;
	}

	/*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
	*/

	setDate(date) {
		this.date = date;
	}

	setSaved(state) {
		this.saved = state;
	}

	setPaused(state) {
		this.paused = state;
	}

	setSpeed(speed) {
		this.speed = speed;
	}

	setGameOver(state) {
		this.gameOver = state;
	}

	setScore(score) {
		this.score = score;

		// Update UI
		$("#score").text(this.getScore());
		$("#xs-score").text(this.getScore());
	}

	increaseScoreBy(amount) {
		this.setScore(this.getScore() + amount);
	}

	setName(name) {
		// If the name is invalid, then do not set new one
		if (!name || name.length <= 0 || name.length > 10 || !/^[a-zA-Z0-9]{1,10}$/.test(name)) return;
		this.name = name;

		// Update UI
		$("#name").val(this.getName());
	}

	/*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function generates random string to act as name of anonymous player.
	 * The logic of generation has source here: https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
	 *
	 * @return {string}
	 */
	randomName() {
		return Math.random().toString(36).substring(3);
	}

	/**
	 * This function generates string representation of current date in following format: 1/12/19 which means: 1st December 2019
	 *
	 * @return {string}
	 */
	getDateString() {
		const date = new Date();
		return date.getDate() + "/" + (date.getMonth() + 1) + "/" + String(date.getFullYear()).substring(2, 4);
	}

	/*
    |--------------------------------------------------------------------------
    | Logic
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function toggles the pause state between true/false
	 */
	togglePause() {
		this.setPaused(!this.isPaused());
	}

	/**
	 * This function resets the state object to initial state
	 *
	 * @param {boolean} paused - Possibility to set the initial paused state
	 */
	reset(paused) {
		// Reset variables
		this.setScore(0);
		this.setPaused(paused);
		this.setGameOver(false);

		// Render leaderboards
		this.renderLeaderboards();

		// Metadata
		this.setSaved(false);
	}

	/*
    |--------------------------------------------------------------------------
    | Leaderboards
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function prepares the data, which it then passes to helper funcitons.
	 * The data will be saved in LocalStorage
	 */
	save() {

		// Prepare data object
		const data = {
			"rank": randInt(10),
			"name": this.getName(),
			"date": this.getDate(),
			"score": this.getScore(),
			"location": undefined
		};

		// Mark this as saved
		this.setSaved(true);

		// Try to get position
		if (navigator.geolocation) navigator.geolocation.getCurrentPosition(pos => {
			// Success. Extract only coordinates
			let position = [pos.coords.latitude, pos.coords.longitude];

			// Save to the data object
			data["location"] = position;

			// Save the data object
			this.saveWithData(data);
		}, (error) => {
			// Error. Save without the location.
			this.saveWithData(data);
		});	
	}

	/**
	 * This function adds passes data, that should be added to the saved object.
	 * Then it renders contents to the UI.
	 *
	 * @param {object} data - Object wich represents one record in leaderboard
	 */
	saveWithData(data) {
		this.append(data);
		this.renderLeaderboards();
	}

	/**
	 * This function renders the leaderboards.
	 */
	renderLeaderboards() {
		// Retrieve leaderboards
		const data = this.getData();

		// Locate table in DOM
		const tbody = $("#leaderboards");

		// Clear the table element
		tbody.empty();

		data.forEach((item, i) => {

			// Create table row
			let tr = $("<tr></tr>");

			// RANK
			const rank = $("<td></td>").text(i + 1);

			// NAME
			const name = $("<td></td>").text(item.name);

			// DATE
			const date = $("<td></td>").text(item.date);

			// SCORE
			const score = $("<td></td>").text(item.score);

			tr.append(rank);
			tr.append(name);
			tr.append(date);
			tr.append(score);

			// Send to UI
			tbody.append(tr);
		});
	}

	/*
    |--------------------------------------------------------------------------
    | Data storage
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function checks whether some data are already saved in LocalStorage
	 *
	 * @return {boolean}
	 */
	dataExists() {
		return !!localStorage.getItem(dataKey);
	}

	/**
	 * This function initializes data to an empty array, if it does not exist
	 */
	initData() {
		if (!this.dataExists()) this.saveData([]);
	}

	/**
	 * This function sorts the records in the array based on score
	 */
	sortData() {
		// Retrieve data from LocalStorage
		let data = this.getData();

		// Sort it
		data.sort((a, b) => {
			let a_score = a.score;
			let b_score = b.score;

			if (a_score > b_score) return -1;
			if (a_score < b_score) return 1;
			return 0;
		});

		// Save it back
		this.saveData(data);
	}

	/**
	 * This function truncates the data, so it only saves 10 best players.
	 */
	truncate() {
		this.saveData(this.getData().filter((item, i) => i < 10));
	}

	/**
	 * This function takes care of adding new data to the saved object.
	 *
	 * @param {object} item - Object wich represents one record in leaderboard
	 */
	append(item) {

		// Retrieve data from LocalStorage
		let data = this.getData();

		// Push new item
		data.push(item);

		// Save it
		this.saveData(data);

		// Sort it
		this.sortData();

		// Truncate it
		this.truncate();
	}
	
	/**
	 * This function directly stores retrieved data object to the LocalStorage.
	 * It also takes care of converting Object to string so it can be saved.
	 *
	 * @param {array} data - Array with leaderboards records
	 */
	saveData(data) {
		localStorage.setItem(dataKey, JSON.stringify(data));
	}

	/**
	 * This retrieves the data from the LocalStorage.
	 * It also tries to initialize the data in case, that it won't be present in LocalStorage.
	 * And when the data are retrieved as string it converts them into an array.
	 *
	 * @return {array} - Array with leaderboards records
	 */
	getData() {
		this.initData();

		// Retrieve data
		return JSON.parse(localStorage.getItem(dataKey));
	}

	/**
	 * This function clears all the data from LocalStorage
	 */
	clearData() {
		if (this.dataExists()) localStorage.removeItem(dataKey);
	}
}