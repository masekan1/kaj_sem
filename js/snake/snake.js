/**
 * This object represents the snake. It consists of Cells, can move and detects collisions.
 */
class Snake {

    constructor(initialSize, state) {

        // Save the initial size of the snake
        this.initialSize = initialSize;

        // Save reference to game state
        this.state = state;

        // Set to initial state
        this.reset();
    }
    
    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    */

    getBody() {
        return this.body;
    }

    getState() {
        return this.state;
    }

    getSpeed() {
        return this.speed;
    }

    getDirection() {
        return this.direction;
    }

    getInitialSize() {
        return this.initialSize;
    }        

    getHead() {
        return this.getBody()[0];
    }

    getLength() {
        return this.getBody().length;
    }

    getTail() {
        return this.getBody()[this.getLength() - 1];
    }

    /**
     * This function returns the direction of the snake as vector.
     * @returns {p5.Vector}
     */
    getDirectionVector() {
        return [new p5.Vector(-1, 0), new p5.Vector(0, -1), new p5.Vector(1, 0), new p5.Vector(0, 1)][this.getDirection() - LEFT_ARROW];
    }

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    setSpeed(speed) {

        // More than 60 is forbidden
        if (speed > 60) return;

        // Save new speed
        this.speed = speed;

        // p5 function frameRate
        frameRate(this.getSpeed());

        // Update UI
        $("#speed").text(this.getSpeed());

        // Forward new speed to the state
        this.getState().setSpeed(this.getSpeed());
    }

    setDirection(direction) {
        // This logic makes sure, that it is not possible to "turn" to opposite direction, than is the current one. (The snake would bit itself)
        this.direction = Math.abs(this.getDirection() - direction) !== 2 ? direction : this.getDirection();
    }

     /*
    |--------------------------------------------------------------------------
    | Logic
    |--------------------------------------------------------------------------
    */

    /**
     * This recalculates cells of the snake to new sizes
     */
    recalculate() {
        this.getBody().forEach(cell => cell.recalculate());
    }

    /**
     * This function increases the spee by specified amount. (Default is 1)
     *
     * @param {number} by - Amount by which the speed will be
     */
    increaseSpeed(by = 1) {
        this.setSpeed(this.getSpeed() + by);
    }

    /**
     * This function determines whether the snake bit itself.
     * It ensures, that the snake is longer than 1 cell.
     * The !! operator is used to quickly convert the expression to boolean
     * Next it takes array with body and strips out the head.
     * Map the array without the head to only p5.Vectors 
     * Try to find any vector, that is the same as head. If it will be successful return that element.
     *
     * @return {boolean}
     */
    bitItself() {
        return this.getLength() > 1 && !!this.getBody().slice(1, this.getLength() - 1).map(cell => cell.getPosition()).find(vector => {
            return vector.equals(this.getHead().getPosition());
        });
    }

    /**
     * This function determines whether the snake ate food.
     *
     * @return {boolean}
     */
    ateFood(food) {
        // Try to find any food, that has the same coordinates as head
        let index = food.findIndex(f => f.getPosition().equals(this.getHead().getPosition()));

        // If none exists, then return false
        if (index < 0) return false;

        // Increase score
        this.getState().increaseScoreBy(food[index].getScore());

        // Increase speed
        if (this.getState().getScore() % 20 === 0) this.increaseSpeed();

        // Increase the length of the snake by one cell
        this.grow();

        // Delete this food
        food.splice(index, 1);

        // Ate food
        return true;
    }

    /**
     * This function takes care of moving the snake.
     * It goes through all cells.
     * Firstly it saves the position of the head and moves the head in desired direction.
     * Next it moves every cell to previous position (stored in tmp) and saves its current position for next cell
     */
    move() {
        let tmp;

        // Go through all cells
        this.getBody().forEach(cell => {

            if (cell.isHead()) {
                // Save the head position
                tmp = cell.getPosition().copy();

                // Move the head
                cell.moveBy(this.getDirectionVector());
            } else {
                // Save the current position of this cell
                let tmp2 = cell.getPosition().copy();

                // Move this cell to the previous position
                cell.moveTo(tmp);

                // Save its previous position for next cell
                tmp = tmp2;
            }
        });
    }

    /**
     * This function takes care of growing
     * It just adds one cell to the last one. It will appear when the last cell moves.
     */
    grow() {
        this.appendCell(new Cell(this.getLength(), this.getTail().getPosition()));
    }

    /**
     * This function adds new cell to the body array
     */
    appendCell(cell) {
        this.body.push(cell);
    }

    /**
     * This function creates new snake with desired length.
     * @return {number} length - The desired length of the snake
     */
    createSnake(length) {

        // Create head
        this.appendCell(new Cell(this.getLength(), new p5.Vector(0, length - 1)));

        // Create body
        for (let index = 0; index < length - 1; index++) {
            this.appendCell(new Cell(this.getLength(), new p5.Vector(0, length - index - 2)));
        }
    }

    /**
     * This function resets the snake to initial state.
     */
    reset() {
        // Reset body
        this.body = [];

        // Reset speed
        this.setSpeed(10);

        // Reset direction
        this.direction = DOWN_ARROW;

        // Create snake
        this.createSnake(this.getInitialSize());
    }

    /*
    |--------------------------------------------------------------------------
    | Canvas
    |--------------------------------------------------------------------------
    */

    /**
     * This function draws the snake.
     */
    draw() {
        this.getBody().forEach(cell => cell.draw());
    }

    /*
    |--------------------------------------------------------------------------
    | Game loop
    |--------------------------------------------------------------------------
    */

    /**
     * Snakes main loop
     */
    update(food, foodCallback) {

        // Move the snake
        this.move();

        // Draw the snake
        this.draw();

        // Call the callback function, with the information whether the snake ate food or not
        foodCallback(this.ateFood(food));

        // Return whether the snake bit itself
        return this.bitItself();
    }
}