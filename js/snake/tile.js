/**
 * Tile represents one tile on the canvas. It store its color and position and takes care of correctly drawing itself.
 */
class Tile {

	constructor(color, position) {

		// Compute the size based on width of canvas and desired tiles per row
		// width is p5 variable, that stores the width of the canvas
		// tilesPerRow is global variable defined in entry.js
		let size = width / tilesPerRow;

		// Create p5.Vector with the size of this tile
		this.size = createVector(size, size);

		// Save the color
		this.color = color;

		// Save the position
		this.position = position;
	}

	/*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
	*/

	/**
     * @returns {p5.Vector}
     */
	getSize() {
		return this.size;
	}

	getColor() {
		return this.color;
	}

	/**
     * @returns {p5.Vector}
     */
	getPosition() {
		return this.position;
	}

	getWidth() {
		return this.getSize().x;
	}

	getHeight() {
		return this.getSize().y;
	}

	getX() {
		return this.getPosition().x;
	}

	getY() {
		return this.getPosition().y;
	}

	/*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
	*/

	/**
     * @param {p5.Vector}
     */
	setPosition(position) {
		this.position = position;
	}

	setX(x) {
		this.position = createVector(x, this.getY());
	}

	setY(y) {
		this.position = createVector(this.getX(), y);
	}

	/*
    |--------------------------------------------------------------------------
    | Logic
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function recalculates the size of the tile
	 */
	recalculate() {
		let size = width / tilesPerRow;
		this.size = createVector(size, size);
	}

	/*
    |--------------------------------------------------------------------------
    | Canvas
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function will draw current tile to the canvas
	 */
	draw() {
		fill(this.getColor());
        rect(this.getX() * this.getWidth(), this.getY() * this.getHeight(), this.getWidth(), this.getHeight());
	}
}