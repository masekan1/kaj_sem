/**
 * This object represents the game itself. It hodl information about the state, snake, food, etc...
 */
class Game {

	constructor(snakeLength) {

		// Load assets
		this.loadSounds();

		// Game state
		this.state = new State();

		// Snake
		this.snake = new Snake(snakeLength, this.state);

		// Put game into initial state
		this.resetGameState();
	}

	/**
	 * This function resets the game state to initial state so it is possible to start again.
	 * The function is intentionally in global scope, because it is a helper function.
	 *
	 * @param {boolean} paused - Possibility to set the initial paused state
	 */
	resetGameState(paused = true) {

		// Reset the Food
		this.food = [];
		this.resetNextFood();

		// Save the background color
		this.backgroundColor = color(0, 0, 0);

		// Reset canvas
		this.clear();

		// Reset state
		this.getState().reset(paused);

		// Reste snake
		this.getSnake().reset();

		// Set up text
		textSize(32);
		textAlign(CENTER, CENTER);
	}

	/**
	 * This function loads the sound effects used in the game.
	 */
	loadSounds() {
		// Load sounds
	    this.sound_pick = loadSound("resources/sounds/pick.wav");
	    this.sound_gameover = loadSound('resources/sounds/gameover.wav');
	}

	/*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
	*/

	getState() {
		return this.state;
	}

	getFood() {
		return this.food;
	}

	getSnake() {
		return this.snake;
	}

	getColor() {
		return this.backgroundColor;
	}

	isPaused() {
		return this.getState().isPaused();
	}

	isSaved() {
		return this.getState().getSaved();
	}

	isGameOver() {
		return this.getState().isGameOver();
	}

	isRunning() {
		return !(this.isPaused() || this.isGameOver());
	}

	/*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
	*/

	setPaused(state) {
		this.getState().setPaused(state);
	}

	setGameOver(state) {
		this.getState().setGameOver(state);
	}

	setDirection(direction) {
		this.getSnake().setDirection(direction);
	}

	/*
    |--------------------------------------------------------------------------
    | Controls
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function toggles the pause state between true/false
	 */
	togglePause() {
		this.getState().togglePause();
	}

	/*
    |--------------------------------------------------------------------------
    | Canvas
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function redraws the canvas. I.e. clears the background.
	 */
	clear() {
		background(this.getColor());
	}

	/**
	 * Draws the food to the canvas
	 */
	drawFood() {
		this.getFood().forEach(tile => tile.draw());
	}

	/*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function returns random position inside canvas
	 *
	 * @return {p5.Vector}
	 */
	randPos() {
		return createVector(randInt(tilesPerRow), randInt(tilesPerRow));
	}

	/**
	 * This function saves the current state to the LocalStorage with help of state object
	 */
	save() {
		this.getState().save();
	}

	/*
    |--------------------------------------------------------------------------
    | Logic
    |--------------------------------------------------------------------------
	*/

	/**
	 * This function sets when the next food should be generated
	 */
	resetNextFood() {
		// It takes current timestamp and adds random number to it
		this.nextFood = Date.now() + randInt(1500) + randInt(2000);
	}

	/**
	 * This function takes care of food generation.
	 */
	updateFood() {
		// Spawn new food. Max 4 foods at one time
		if (this.getFood().length < 4 && Date.now() > this.nextFood) {
			// Create new food
			this.getFood().push(new Food(this.randPos()));

			// Reset the timer
			this.resetNextFood();
		}

		// Draw food
		this.drawFood();
	}

	/**
	 * This function causes to recalculate dimesions of tiles
	 */
	recalculate() {
		this.getSnake().recalculate();
		this.getFood().forEach(f => f.recalculate());
	}

	/*
    |--------------------------------------------------------------------------
    | Audio
    |--------------------------------------------------------------------------
	*/

	/**
	 * Plays the picking sound  if not muted.
	 */
	pickAudio() {
		if (sounds) this.sound_pick.play();
	}

	/**
	 * Plays the game over sound  if not muted.
	 */
	gameOverAudio() {
		if (sounds) this.sound_gameover.play();
	}

	/*
    |--------------------------------------------------------------------------
    | Game loop
    |--------------------------------------------------------------------------
	*/

	/**
	 * Main game loop
	 */
	update() {
		// Clear the canvas
		this.clear();

		// Update food
		this.updateFood();

		// Update the snake
		// The snake.update method returns boolean which determines whether the snake bit itself -> game over
		// Also it takes in two arguments: array of food and callback, which takes care of playing the sound of eating.
		this.setGameOver(this.snake.update(this.getFood(), didEat => {
			if (didEat) this.pickAudio();
		}));
	}
}