/**
 * Cell represents one piece of the body of the snake. 
 */
class Cell {

    constructor(index, position) {

        // Save index and position of current cecll
        this.index = index;
        this.tile = new Tile(color(255, 255, 255), position);
    }

    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    */

    getTile() {
        return this.tile;
    }

    getIndex() {
        return this.index;
    }

    isHead() {
        return !this.getIndex();
    }

    /**
     * @returns {p5.Vector} Returns the position of this cell
     */
    getPosition() {
        return this.getTile().getPosition();
    }

    /*
    |--------------------------------------------------------------------------
    | Logic
    |--------------------------------------------------------------------------
    */

    /**
     * This function will tell the tile to recalculate its size
     */
    recalculate() {
        this.getTile().recalculate();
    }

    /**
     * Moves this cell to spcified position
     * 
     * @param {p5.Vector} newPosition - Position to which this cell will be moved
     */
    moveTo(newPosition) {
        this.getTile().setPosition(newPosition);
    }

    /**
     * Moves this cell by specified offset
     * 
     * @param {p5.Vector} vector - Offst by which this cell will be moved
     */
    moveBy(vector) {
        this.getTile().setX((this.getTile().getX() + vector.x).mod(tilesPerRow));
        this.getTile().setY((this.getTile().getY() + vector.y).mod(tilesPerRow));
    }

    /*
    |--------------------------------------------------------------------------
    | Canvas
    |--------------------------------------------------------------------------
    */

    /**
     * Draws the cell to the canvas.
     */
    draw() {
        this.getTile().draw();
    }
}