/*
|--------------------------------------------------------------------------
| Global variables
|--------------------------------------------------------------------------
*/

/** @type {boolean} */
let sounds = true;

/** @const {number} */
const tilesPerRow = 30;

/** @const {string} */
const dataKey = "leaderboards";

/** @type {boolean} */
let allowChageDirection = true;

/*
|--------------------------------------------------------------------------
| Helper methods
|--------------------------------------------------------------------------
*/

/**
 * This function returns the size, that the canvas will have. It is derived from the size of the div inside which the canvas will be.
 *
 * @return {number} - The size of the side of the canvas 
 */
function getSize() {
    const size = $("#col-game").width();
    return size > 700 ? 700 : size;
}

/**
 * This function returns random number in range <0, max>.
 * The function is intentionally in global scope, because it is a helper function.
 *
 * @param {number} max - Upper bound
 * @return {number} 
 */
function randInt(max) {
    return Math.floor(Math.random() * (max + 1));
}

/*
|--------------------------------------------------------------------------
| Setup
|--------------------------------------------------------------------------
*/

/**
 * This function is entry point of p5 framework. 
 */
function setup() {
    // Create canvas
    let canvas = createCanvas(getSize(), getSize());

    // Set the font face
    textFont("NeonFont");

    // Put the canvas into a div
    canvas.parent("snake-frame");

    // Create new game object instance
    this.game = new Game(5); 
}

/*
|--------------------------------------------------------------------------
| Loop
|--------------------------------------------------------------------------
*/

/**
 * This function also comes from p5 framework. It basically is the main loop.
 */
function draw() {
    // Determine whether the game is running and should be updated
    if (this.game.isRunning())  this.game.update();
    else this.game.clear();

    // Set the font color
    fill(0, 183, 235);    

    // If the game is paused, then show corresponding message
    if (this.game.isPaused()) text("Press Enter", width / 2, height / 2);
    else if (this.game.isGameOver()) {

        // If the game is in GameOver state, then show info
        text("Game Over!", width / 2, height / 2 - textSize() / 2);
        text("Press Enter to restart", width / 2, height / 2 + textSize() - textSize() / 2);

        // Save to the local storage
        if (!this.game.isSaved()) {
            // This block of code will run only once
            this.game.save();

            // Play GameOver sound
            this.game.gameOverAudio();
        }
    }

    // This is here so the user can change the direction only once per frame
    enableControls();
}

/*
|--------------------------------------------------------------------------
| Keyboard input
|--------------------------------------------------------------------------
*/

/**
 * This function returns whether the user is able to control the snake.
 * 
 * @return {boolean} 
 */
function canControl() {
    return allowChageDirection;
}

 /**
 * This function disables controls
 */
function disableControls() {
    setAllowInput(false);
}

/**
 * This function enables controls
 */
function enableControls() {
    setAllowInput(true);
}

/**
 * This function sets the enable/disable state of controls
 */
function setAllowInput(state) {
    allowChageDirection = state;
}

/**
 * This function is triggered every time some key was pressed. It is part of p5 library.
 * Also here is the logic of controlling the sake.
 */
function keyPressed() {
    // Controlling the game (keyCode == 13 means Enter)
    if (keyCode == 13 && !this.game.isGameOver()) this.game.togglePause();
    else if (keyCode == 13 && this.game.isGameOver()) this.game.resetGameState(false); 

    // Mute (keyCode == 77 means key M)
    if (keyCode == 77) sounds = !sounds;

    // Return from this fuction if controlling the snake if forbidden
    if (!canControl()) return;

    // Check if some arrow key was pressed
    if ([LEFT_ARROW, UP_ARROW, RIGHT_ARROW, DOWN_ARROW].includes(keyCode)) {
        // Change the direciton
        this.game.setDirection(keyCode);

        // Disable arrows until next frame
        disableControls();
    }
}

/*
|--------------------------------------------------------------------------
| Events
|--------------------------------------------------------------------------
*/

/**
 * Register `resize` event on window, so it will do necessary operations when resizing the window.
 * Since the size of the canvas is changing, so are the sizes of tiles.  Because of this the game is paused during the resizing process.
 * Even if there is some basic functionality to change the size of the canvas, it is better, when the window size will stay the same during entire gameplay.
 * It is possible to load the game on smaller screens, but at the moment the touch input is not yet implemented.
 */
$(window).resize(() => {
    // When the window is being resized, then pause the game
    this.game.setPaused(true);

    // Resize the canvas
    resizeCanvas(getSize(), getSize());

    // Recalculate sizes
    this.game.recalculate();
});

$(document).ready(() => {

    /**
     * When the contents of #name input chagnes, then update the name in State object
     */
    $("#name").change((e) => {
        this.game.getState().setName(e.target.value);
    });

    // Initial check of the Online/Offline state
    if (navigator.onLine) $("#status-sign").addClass("online");
    else $("#status-sign").removeClass("online");
});

/**
* Register an `online` event to reflect the current state on the page
*/
$(window).on("online", () => {
    $("#status-sign").addClass("online");
});

/**
* Register an `offline` event to reflect the current state on the page
*/
$(window).on("offline", () => {
    $("#status-sign").removeClass("online");
});